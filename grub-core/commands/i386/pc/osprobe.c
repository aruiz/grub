/* osprobe.c -- Searches for other operating systems installed on the system.
 */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2020  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This command iterates over every dist/partition and tries to find bootable
 * operating systems by searching for boot partitions and adding menu entries.
 * It infers the target operating system by a combination of partition type,
 * filesystem type and filesystem contents depending on each case.
 *
 * This implementation targets the x86/x86_64 legacy BIOS usecase, it does not
 * cover EFI or other platforms.
 *
 * The motivation for this command is to avoid using os-prober at install time
 * which tries to mount everything and parse files internal to each operating
 * system. This has proven unreliable and fragile.
 */

/*
 * TODO:
 * - Figure out how to boot from filesystems that take the entire disk
 * - Figure out how to chainload FreeBSD with ZFS root (both MBR and GPT)
 * - Explore how to boot a legacy windows volume that changed its BIOS order
 * - Test with empty removable media (reading mbr filed with empty floppy
 *   drives, need to test if this is a general issue)
 */

#include <grub/command.h>
#include <grub/device.h>
#include <grub/dl.h>
#include <grub/env.h>
#include <grub/err.h>
#include <grub/extcmd.h>
#include <grub/file.h>
#include <grub/fs.h>
#include <grub/gpt_partition.h>
#include <grub/i18n.h>
#include <grub/lib/arg.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/msdos_partition.h>
#include <grub/normal.h>
#include <grub/partition.h>

GRUB_MOD_LICENSE ("GPLv2+");

#define UNUSED __attribute__ ((unused))

#define PATH_BUF_SIZE 1024

#define MBR_LINUX_CODE 0x83
#define MBR_SOLARIS_PART_CODE 0xbf
#define MBR_FREEBSD_PART_CODE 0xa5
#define MBR_OPENBSD_PART_CODE 0xa6
#define MBR_NETBSD_PART_CODE 0xa9
#define MBR_HAIKU_PART_CODE 0xeb
#define MBR_EFI_PART_CODE 0xef

#define MBR_BOOT_FLAG 0x80

#define PROBE_FUNCTION(FNAME, P_LAYOUT)                                       \
  static grub_uint8_t probe_##FNAME (const char *part_device_name)            \
  {                                                                           \
    const char *lyt[] = P_LAYOUT;                                             \
    return probe_layout (part_device_name, lyt);                              \
  }
#define WIN95_ME_LAYOUT                                                       \
  {                                                                           \
    "IO.SYS", NULL                                                            \
  }
#define DOS_LAYOUT                                                            \
  {                                                                           \
    "MSDOS.SYS", NULL                                                         \
  }
#define BOOTMGR_LAYOUT                                                        \
  {                                                                           \
    "bootmgr", NULL                                                           \
  }
#define NT_LAYOUT                                                             \
  {                                                                           \
    "ntldr", NULL                                                             \
  }
#define FREEDOS_LAYOUT                                                        \
  {                                                                           \
    "KERNEL.SYS", NULL                                                        \
  }
#define FREEBSD_LAYOUT                                                        \
  {                                                                           \
    "boot/loader", NULL                                                       \
  }
#define NETBSD_LAYOUT                                                         \
  {                                                                           \
    "netbsd", NULL                                                            \
  }
#define OPENBSD_LAYOUT                                                        \
  {                                                                           \
    "bsd", NULL                                                               \
  }
#define GRUB_2_LAYOUT                                                         \
  {                                                                           \
    "boot/grub/i386-pc/boot.img", "boot/grub2/i386-pc/boot.img",              \
        "grub/i386-pc/boot.img", "grub2/i386-pc/boot.img", NULL               \
  }
#define GRUB_1_LAYOUT                                                         \
  {                                                                           \
    "boot/grub/menu.lst", "grub/menu.lst", NULL                               \
  }

enum partition_label_t
{
  PART_TYPE_UNKNOWN = 0,
  PART_TYPE_MBR,
  PART_TYPE_GPT,
};

const grub_gpt_part_guid_t GPT_GUID_ESP
    = { grub_cpu_to_le32_compile_time (0xc12a7328),
        grub_cpu_to_le16_compile_time (0xf81f),
        grub_cpu_to_le16_compile_time (0x11d2),
        { 0xba, 0x4b, 0x00, 0xa0, 0xc9, 0x3e, 0xc9, 0x3b } };

const grub_gpt_part_guid_t GPT_GUID_FREEBSD
    = { grub_cpu_to_le32_compile_time (0x516E7CB6),
        grub_cpu_to_le16_compile_time (0x6ECF),
        grub_cpu_to_le16_compile_time (0x11D6),
        { 0x8F, 0xF8, 0x00, 0x02, 0x2D, 0x09, 0x71, 0x2B } };

const grub_gpt_part_guid_t GPT_GUID_NETBSD
    = { grub_cpu_to_le32_compile_time (0x49F48D5A),
        grub_cpu_to_le16_compile_time (0xB10E),
        grub_cpu_to_le16_compile_time (0x11DC),
        { 0xB9, 0x9B, 0x00, 0x19, 0xD1, 0x87, 0x96, 0x48 } };

const grub_gpt_part_guid_t GPT_GUID_HAIKU
    = { grub_cpu_to_le32_compile_time (0x42465331),
        grub_cpu_to_le16_compile_time (0x3BA3),
        grub_cpu_to_le16_compile_time (0x10F1),
        { 0x80, 0x2A, 0x48, 0x61, 0x69, 0x6B, 0x75, 0x21 } };

static grub_uint8_t find_files (const char *, const char **);

static grub_uint8_t
probe_layout (const char *part_device_name, const char *layout[])
{
  return find_files (part_device_name, layout);
}

/* We generate a probing function per each platform we want to probe */
PROBE_FUNCTION (w95_me, WIN95_ME_LAYOUT);
PROBE_FUNCTION (dos, DOS_LAYOUT);
PROBE_FUNCTION (freedos, FREEDOS_LAYOUT);
PROBE_FUNCTION (nt, NT_LAYOUT);
PROBE_FUNCTION (ms_bootmgr, BOOTMGR_LAYOUT);
PROBE_FUNCTION (netbsd, NETBSD_LAYOUT);
PROBE_FUNCTION (freebsd, FREEBSD_LAYOUT);
PROBE_FUNCTION (openbsd, OPENBSD_LAYOUT);

static char
compare_guid (const grub_gpt_part_guid_t *a, grub_gpt_part_guid_t *b)
{
  return a->data1 == b->data1 && a->data2 == b->data2 && a->data3 == b->data3
         && a->data4[0] == b->data4[0] && a->data4[1] == b->data4[1]
         && a->data4[2] == b->data4[2] && a->data4[3] == b->data4[3]
         && a->data4[4] == b->data4[4] && a->data4[5] == b->data4[5]
         && a->data4[6] == b->data4[6] && a->data4[7] == b->data4[7];
}

static void
get_partition_device_name (const char *disk_name,
                           const grub_partition_t partition,
                           char *partition_device_name)
{
  char *partition_name = grub_partition_get_name (partition);
  grub_snprintf (partition_device_name, PATH_BUF_SIZE, "%s,%s", disk_name,
                 partition_name);
  grub_free (partition_name);
}

static const char *
get_fs_name (const char *device_name)
{
  grub_device_t partdevice = grub_device_open (device_name);

  if (partdevice == NULL)
    return NULL;

  grub_fs_t part_fs = grub_fs_probe (partdevice);
  grub_device_close (partdevice);

  return part_fs->name;
}

static grub_uint8_t
find_single_file (const char *path)
{
  grub_uint8_t ret;

  grub_error_push ();
  grub_file_t file = grub_file_open (path, GRUB_FILE_TYPE_FS_SEARCH);
  grub_error_pop ();

  ret = file ? 1 : 0;

  if (file)
    grub_file_close (file);

  return ret;
}

static grub_uint8_t
find_files (const char *device, const char **paths)
{
  grub_uint8_t ret = 1;
  for (char **path = (char **)paths; *path; path++)
    {
      char filename[PATH_BUF_SIZE];
      grub_snprintf (filename, PATH_BUF_SIZE, "(%s)/%s", device, *path);
      if (!find_single_file (filename))
        {
          ret = 0;
          break;
        }
    }
  return ret;
}

static void
menu_add_chainloader (const char *title, const char *partition,
                      const char *src)
{
  char full_title[PATH_BUF_SIZE];
  grub_snprintf (full_title, PATH_BUF_SIZE, "%s (%s)", title, partition);
  const char *chain_args[1] = { full_title };
  grub_normal_add_menu_entry (sizeof chain_args, chain_args, NULL, NULL, NULL,
                              NULL, NULL, src, 0);
}

static void
menu_add_grub2_chainloader (const char *title, const char *device,
                            const char *core)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE, "set root=(%s)\nchainloader --force %s",
                 device, core);
  menu_add_chainloader (title, device, (const char *)src);
}

static void
menu_add_grub1_chainloader (const char *title, const char *device,
                            const char *menulst)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE,
                 "set root=(%s)\ninsmod legacycfg\nlegacy_configfile %s",
                 device, menulst);
  menu_add_chainloader (title, device, (const char *)src);
}

static void
menu_add_legacy_chainloader (const char *title, const char *device)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE, "set root=(%s)\nchainloader +1", device);
  menu_add_chainloader (title, device, (const char *)src);
}

static void
menu_add_ntldr_chainloader (const char *title, const char *device)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE, "set root=(%s)\nntldr /ntldr", device);
  menu_add_chainloader (title, device, (const char *)src);
}

static void
menu_add_bootmgr_chainloader (const char *title, const char *device)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE, "set root=(%s)\nntldr /bootmgr", device);
  menu_add_chainloader (title, device, (const char *)src);
}

static void
menu_add_freebsd_loader (const char *device)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE,
                 "insmod bsd\nset root=(%s)\nkfreebsd /boot/loader", device);
  menu_add_chainloader ("FreeBSD", device, (const char *)src);
}

static void
menu_add_netbsd_loader (const char *device)
{
  char src[PATH_BUF_SIZE];
  grub_snprintf (src, PATH_BUF_SIZE,
                 "insmod bsd\nset root=(%s)\nknetbsd /netbsd", device);
  menu_add_chainloader ("NetBSD", device, (const char *)src);
}

static grub_uint8_t
probe_grub (const char *part_device_name, char buffer[PATH_BUF_SIZE],
            const char *GRUB_PROBE_FILES[])
{
  for (char **path = (char **)GRUB_PROBE_FILES; *path; path++)
    {
      int len = grub_snprintf (buffer, PATH_BUF_SIZE, "(%s)/%s",
                               part_device_name, *path);

      if (len == PATH_BUF_SIZE)
        {
          grub_printf ("Path exceeded buffer size of %d bytes: '(%s)/%s'",
                       PATH_BUF_SIZE, part_device_name, *path);
          continue;
        }

      if (find_single_file (buffer))
        {
          return 1;
        }
    }

  buffer[0] = 0;
  return 0;
}

static int
iterate_partitions (grub_disk_t disk, const grub_partition_t partition,
                    struct grub_msdos_partition_mbr *mbr)
{
  char part_device_name[PATH_BUF_SIZE];
  get_partition_device_name (disk->name, partition, part_device_name);

  const char *fs_name = get_fs_name (part_device_name);
  char is_fat = !grub_strcmp (fs_name, "fat");
  char is_ntfs = !grub_strcmp (fs_name, "ntfs");

  enum partition_label_t pttype = PART_TYPE_UNKNOWN;
  struct grub_gpt_partentry gpt_entry;
  const struct grub_msdos_partition_entry *mbr_entry = NULL;
  grub_errno = GRUB_ERR_NONE;

  if (!grub_strcmp (partition->partmap->name, "msdos"))
    {
      pttype = PART_TYPE_MBR;
      mbr_entry = &mbr->entries[partition->index];
      if (is_fat && mbr->entries[partition->index].type == MBR_EFI_PART_CODE)
        {
          return 0; // ESP
        }
    }
  else if (!grub_strcmp (partition->partmap->name, "gpt"))
    {
      pttype = PART_TYPE_GPT;
      const grub_gpt_part_guid_t esp = GPT_GUID_ESP;

      if (grub_disk_read (disk, partition->offset, partition->index,
                          sizeof (gpt_entry), &gpt_entry))
        {
          return 0; // TODO: LOG ERROR
        }

      if (!grub_strcmp (fs_name, "fat")
          && compare_guid ((grub_gpt_part_guid_t *)&esp, &gpt_entry.type))
        {
          return 0; // ESP
        }
    }
  else
    {
      return 0; // UNKNOWN partition header, this should not be reached
    }

  /* DOS/Windows detection block */
  if ((pttype == PART_TYPE_MBR || (pttype == PART_TYPE_GPT))
      && (is_fat || is_ntfs))
    {
      if (probe_ms_bootmgr (part_device_name))
        menu_add_bootmgr_chainloader ("Windows Vista/7/8/10",
                                      part_device_name);
      else if (probe_nt (part_device_name))
        menu_add_ntldr_chainloader ("Windows NT/2000/XP", part_device_name);
      else if (probe_w95_me (part_device_name))
        menu_add_legacy_chainloader ("Windows 95/98/ME", part_device_name);
      else if (probe_dos (part_device_name))
        menu_add_legacy_chainloader ("MS-DOS", part_device_name);
      else if (probe_freedos (part_device_name))
        menu_add_legacy_chainloader ("FreeDOS", part_device_name);

      return 0;
    }

  if (pttype == PART_TYPE_MBR && mbr_entry)
    {
      char path[PATH_BUF_SIZE] = { 0 };
      switch (mbr_entry->type)
        {
        case MBR_LINUX_CODE:
          if (probe_grub (part_device_name, path,
                          (const char *[])GRUB_2_LAYOUT))
            menu_add_grub2_chainloader ("Linux/GRUB2", part_device_name, path);
          else if (probe_grub (part_device_name, path,
                               (const char *[])GRUB_1_LAYOUT))
            menu_add_grub1_chainloader ("Linux/GRUB Legacy", part_device_name,
                                        path);
          break;
        case MBR_OPENBSD_PART_CODE:
          if (probe_openbsd (part_device_name))
            menu_add_legacy_chainloader ("OpenBSD", part_device_name);
          break;
        case MBR_NETBSD_PART_CODE:
          if (probe_netbsd (part_device_name))
            menu_add_legacy_chainloader ("NetBSD", part_device_name);
          break;
        case MBR_FREEBSD_PART_CODE:
          if (probe_freebsd (part_device_name))
            menu_add_legacy_chainloader ("FreeBSD", part_device_name);
          break;
        case MBR_SOLARIS_PART_CODE:
          if (mbr_entry->flag == MBR_BOOT_FLAG)
            menu_add_legacy_chainloader ("OpenIndiana/Solaris",
                                         part_device_name);
          break;
        case MBR_HAIKU_PART_CODE:
          if (mbr_entry->flag == MBR_BOOT_FLAG
              && !grub_strcmp (fs_name, "bfs"))
            menu_add_legacy_chainloader ("HaikuOS/BeOS", part_device_name);
          break;
        default:
          break;
        }
    }
  else if (pttype == PART_TYPE_GPT)
    { /* Generic GPT block */
      if (compare_guid (&GPT_GUID_FREEBSD, &gpt_entry.type)
          && probe_freebsd (part_device_name))
        menu_add_freebsd_loader (part_device_name);
      else if (compare_guid (&GPT_GUID_NETBSD, &gpt_entry.type)
               && probe_netbsd (part_device_name))
        menu_add_netbsd_loader (part_device_name);
      else if (compare_guid (&GPT_GUID_HAIKU, &gpt_entry.type)
               && !grub_strcmp (fs_name, "bfs"))
        menu_add_legacy_chainloader ("Haiku/BeOS", part_device_name);
    }

  return 0;
}

static int
iterate_disks (const char *name, grub_disk_t root)
{
  grub_disk_t disk;
  struct grub_msdos_partition_mbr mbr;

  /* Skipping floppy disks */
  if (name[0] == 'f' && name[1] == 'd')
    return 0;

  if (!(disk = grub_disk_open (name)))
    {
      grub_printf (N_ ("Could not open disk %s"), name);
      return 0;
    }

  if (root && root->id == disk->id)
    return 0;

  if (grub_disk_read (disk, 0, 0, sizeof (struct grub_msdos_partition_mbr),
                      &mbr))
    return 0;

  grub_partition_iterate (
      disk, (grub_partition_iterate_hook_t)iterate_partitions, &mbr);

  grub_disk_close (disk);
  return 0;
}

/* Global variables */
static grub_disk_t   root;
static grub_extcmd_t os_probe;

static grub_err_t
grub_cmd_osprobe (grub_extcmd_context_t ctxt UNUSED, int argc UNUSED,
                  char **args UNUSED)
{
  grub_dl_load ("part_gpt");
  grub_dl_load ("part_msdos");

  grub_disk_dev_iterate ((grub_disk_dev_iterate_hook_t)iterate_disks, root);
  grub_disk_close (root);

  return GRUB_ERR_NONE;
}

GRUB_MOD_INIT (osprobe)
{
  root = grub_disk_open (grub_env_get ("root")); /* We initialize root here to avoid problems if 'set root=...' is called later on */
  os_probe = grub_register_extcmd (
      "osprobe", grub_cmd_osprobe, 0,
      N_ ("Searches for other operating systems installed on the system."),
      N_ ("Searches for other operating systems installed on the system."),
      NULL);
}

GRUB_MOD_FINI (osprobe) { grub_unregister_extcmd (os_probe); }
